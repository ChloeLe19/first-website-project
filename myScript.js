function openNavBar() {
  var x = document.getElementById("navbar");
  if (x.className === "navbar") {
    x.className += " responsive";
  } else {
    x.className = "navbar";
  }
}

function onCloseNavBar() {
  var x = document.getElementById("navbar");
  x.classList.remove("responsive")
}


document.querySelectorAll(".mobilelink").forEach(el => {
  el.addEventListener('click', function (event) {
    if (event.target.scrollIntoView) {
      event.preventDefault()
      var hash = event.target.attributes.href.nodeValue
      document.querySelector(hash).scrollIntoView({behavior: "smooth"});
      if(history.pushState) {
        history.pushState(null, null,  hash);
      }

    }
    onCloseNavBar();
  })
});

document.querySelector("#closeMobileNavi_btn").addEventListener('click', onCloseNavBar)


document.getElementById("openMobileNavi_btn").addEventListener("click", function () {
  openNavBar()
})


function showThankyou() {
  document.querySelector(".form-container .form-inner").style.display = "none"
  document.querySelector(".form-container .thank_you").style.display = "block"
}

function hideThankyou() {
  document.querySelector(".form-container .form-inner").style.display = "flex"
  document.querySelector(".form-container .thank_you").style.display = "none"
}


function emptyForm(form) {
  var inputs = form.querySelectorAll("input, textarea").forEach(el => {
    el.value = "";
    el.setCustomValidity("");
  });
}


function checkRequired(form) {
  var inputs = form.querySelectorAll("input:required");
  var ret = true
  inputs.forEach(function (el) {
    if (el.value === "") {
      el.setCustomValidity("Invalid field.");
      ret = false
    }
  })
  return ret;
}

document.querySelector("#send_form").addEventListener('click', function (e) {
  e.preventDefault()

  var form = document.getElementById('contact_form')
  if (!checkRequired(form)) {
    return
  }


  var data = new FormData(form)
  data.append("juntti", "puntti")
  axios.post("rhdanke.php", data).then(function () {
    showThankyou()
    setTimeout(function () {
      hideThankyou()
      emptyForm(form)
    }, 13000)
  })
})


function q(string) {
  return document.querySelector(string)
}

q("#impressum .close").addEventListener('click', function () {
  q("#impressum").style.display = "none"
})

q(".open_impressum").addEventListener('click', function () {
  q("#impressum").style.display = "block"
})
